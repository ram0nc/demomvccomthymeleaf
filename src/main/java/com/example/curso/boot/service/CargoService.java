

package com.example.curso.boot.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.curso.boot.domain.Cargo;
import com.example.curso.boot.repository.CargoRepository;

@Service
public class CargoService {

	@Autowired
	private CargoRepository repo;
	
	@Transactional
	public void salvar(Cargo cargo) {
		repo.save(cargo);
	}
	@Transactional
	public void editar(Cargo cargo) {
		repo.save(cargo);
	}
	@Transactional
	public void excluir(Integer id) {
		repo.deleteById(id);
	}
	

	public Optional<Cargo> buscarPorId(Integer id) {
		Optional<Cargo> cargo = repo.findById(id);
		return cargo;
	}
	
	public List<Cargo> buscarTodos() {

		return repo.findAll();
	}
	
	public boolean cargoTemFuncionario(Integer id) {
		if(buscarPorId(id).get().getFuncionarios().isEmpty()) {
			return false;
		}
		return true;
	}
}
