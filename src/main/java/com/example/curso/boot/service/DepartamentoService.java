package com.example.curso.boot.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.curso.boot.domain.Departamento;
import com.example.curso.boot.repository.DepartamentoRepository;

@Service
public class DepartamentoService {

	@Autowired
	private DepartamentoRepository repo;

	@Transactional
	public void salvar(Departamento departamento) {
		repo.save(departamento);
	}

	@Transactional
	public void editar(Departamento departamento) {
		repo.save(departamento);
	}

	@Transactional
	public void excluir(Long id) {
		repo.deleteById(id);
	}

	public Optional<Departamento> buscarPorId(Long id) {
		Optional<Departamento> departamento = repo.findById(id);

		return departamento;
	}

	public List<Departamento> buscarTodos() {

		return repo.findAll();
	}

	public boolean departamentoTemCargos(Long id) {

		Departamento departamento = buscarPorId(id).get();
		if (departamento != null && departamento.getCargos().isEmpty()) {
			return false;
		}
		return true;
	}
}
