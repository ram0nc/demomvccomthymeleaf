package com.example.curso.boot.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import com.example.curso.boot.domain.Funcionario;
import com.example.curso.boot.repository.FuncionarioRepository;
@Service
public class FuncionarioService {

	@Autowired
	private FuncionarioRepository repo;
	
	@Transactional
	public void salvar(Funcionario funcionario) {
		repo.save(funcionario);
	}
	@Transactional
	public void editar(Funcionario funcionario) {
		repo.save(funcionario);
	}
	@Transactional
	public void excluir(Integer id) {
		repo.deleteById(id);
	}
	

	public Optional<Funcionario> buscarPorId(Integer id) {
		Optional<Funcionario> funcionario = repo.findById(id);
		return funcionario;
	}
	
	public List<Funcionario> buscarTodos() {

		return repo.findAll();
	}
	public List<Funcionario> buscarPorNome(String nome) {
		// TODO Auto-generated method stub
		return repo.findByNomeContaining(nome);
	}
	
	public List<Funcionario> buscarPorCargo(Integer id) {
		// TODO Auto-generated method stub
		return repo.findByCargoId(id);
	}
	public List<Funcionario> buscarPorDatas(LocalDate entrada, LocalDate saida) {
		// TODO Auto-generated method stub
	if (entrada != null && saida != null) {
		return repo.findByDataEntradaAfterAndDataSaidaBefore(entrada,saida);
	} else if(entrada != null) {
		return repo.findByDataEntrada(entrada);
	} else if(saida != null) {
		return repo.findByDataSaida(saida);
	} else {
		return new ArrayList<>();
	}
	
	}
}
