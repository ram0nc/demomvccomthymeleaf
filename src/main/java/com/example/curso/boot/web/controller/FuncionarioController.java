package com.example.curso.boot.web.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.curso.boot.domain.Cargo;
import com.example.curso.boot.domain.Funcionario;
import com.example.curso.boot.domain.UF;
import com.example.curso.boot.service.CargoService;
import com.example.curso.boot.service.FuncionarioService;
import com.example.curso.boot.web.validator.FuncionarioValidator;

@Controller
@RequestMapping("/funcionarios")
public class FuncionarioController {

	@Autowired
	private FuncionarioService funcionarioService;
	
	@Autowired
	private CargoService cargoService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new FuncionarioValidator());
	}
	
	@GetMapping("/cadastrar")
	public String cadastrar(Funcionario funcionario) {
		return "/funcionario/cadastro";
	}

	@GetMapping("/listar")
	public String listar(ModelMap model) {
		
		model.addAttribute("funcionarios", funcionarioService.buscarTodos());
		model.addAttribute("mensagem", "Você realmente deseja excluir este Funcionario?");
		return "/funcionario/lista";
	}
	
	@PostMapping("/salvar")
	public String salvar(@Valid Funcionario funcionario,  BindingResult result,RedirectAttributes attr){
		if(result.hasErrors()) {
			return "/funcionario/cadastro";
		}
		
		funcionarioService.salvar(funcionario);
		attr.addFlashAttribute("success", "funcionario inserido com sucesso");
		return "redirect:/funcionarios/listar";
	}
	
	@ModelAttribute("cargos")
	public List<Cargo> getCargos() {
		return cargoService.buscarTodos();
		
	}
	
	@ModelAttribute("ufs")
	public UF[] getUFs() {
		return UF.values();
	}

	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Integer id, ModelMap model) {
	
		if( funcionarioService.buscarPorId(id).isEmpty()) {
			model.addAttribute("fail", "Funcionario não existe");
			return listar(model);
		} else {
			
		model.addAttribute("funcionario", funcionarioService.buscarPorId(id).get());
		return "/funcionario/cadastro";
		}
	}
	
	@PostMapping("/editar")
	public String editar(@Valid Funcionario funcionario,  BindingResult result,RedirectAttributes attr){
		if(result.hasErrors()) {
			return "/funcionario/cadastro";
		}
		
		if(funcionario.getId() == null) {
			attr.addAttribute("fail", "Funcionario não existe");
		}else if( funcionarioService.buscarPorId(funcionario.getId()) == null) {
			attr.addAttribute("fail", "Funcionario não existe");
		}
		else {
			funcionarioService.editar(funcionario);
			attr.addFlashAttribute("success", "Funcionario editado com sucesso");
		}
		return "redirect:/funcionarios/listar";
	}
	
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Integer id, ModelMap model) {
		if(funcionarioService.buscarPorId(id).isEmpty()) {
			model.addAttribute("fail", "Funcionario não existe");
		}else {
		
		
			funcionarioService.excluir(id);
			model.addAttribute("success", "Funcionario removido.");
		}
		return listar(model);
	}
	
	@GetMapping("/buscar/nome")
	public String getPorNome(@RequestParam("nome") String nome, ModelMap model) {
		model.addAttribute("funcionarios", funcionarioService.buscarPorNome(nome));
		model.addAttribute("mensagem", "Você realmente deseja excluir este Funcionario?");

		return "/funcionario/lista";
	}
	
	@GetMapping("/buscar/cargo")
	public String getPorNome(@RequestParam("id") Integer id, ModelMap model) {
		model.addAttribute("funcionarios", funcionarioService.buscarPorCargo(id));
		model.addAttribute("mensagem", "Você realmente deseja excluir este Funcionario?");

		return "/funcionario/lista";
	}
	
	@GetMapping("/buscar/data")
	public String getPorDatas(@RequestParam("entrada") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate entrada, 
								@RequestParam("saida") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate saida, 
								ModelMap model) {
		model.addAttribute("funcionarios", funcionarioService.buscarPorDatas(entrada,saida));
		model.addAttribute("mensagem", "Você realmente deseja excluir este Funcionario?");

		return "/funcionario/lista";
	}
}
