package com.example.curso.boot.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.curso.boot.domain.Cargo;
import com.example.curso.boot.domain.Departamento;
import com.example.curso.boot.service.CargoService;
import com.example.curso.boot.service.DepartamentoService;

@Controller
@RequestMapping("/cargos")
public class CargoController {

	@Autowired
	private CargoService cargoService;
	
	@Autowired
	private DepartamentoService departamentoService;
	
	
	@GetMapping("/cadastrar")
	public String cadastrar(Cargo cargo) {
		return "/cargo/cadastro";
	}
	
	@GetMapping("/listar")
	public String listar(ModelMap model) {
		model.addAttribute("cargos", cargoService.buscarTodos());
		model.addAttribute("mensagem", "Você realmente deseja excluir este cargo?");
		return "/cargo/lista";
	}
	
	@PostMapping("/salvar")
	public String salvar(@Valid Cargo cargo, BindingResult result, RedirectAttributes attr) {
		
		if(result.hasErrors()) {
			return "/cargo/cadastro";
		}
		cargoService.salvar(cargo);
		attr.addFlashAttribute("success", "cargo inserigo com sucesso.");
		return "redirect:/cargos/listar";
	}
	
	@ModelAttribute("departamentos")
	public List<Departamento> listaDeDepartamentos(){
		return departamentoService.buscarTodos();
	}
	
	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Integer id, ModelMap model) {
	
		if( cargoService.buscarPorId(id).isEmpty()) {
			model.addAttribute("fail", "Cargo não existe");
			return listar(model);
		} else {
		model.addAttribute("cargo", cargoService.buscarPorId(id).get());
		return "/cargo/cadastro";
		}
	}
	
	@PostMapping("/editar")
	public String editar(@Valid Cargo cargo, BindingResult result, RedirectAttributes attr){
		
		if(result.hasErrors()) {
			return "/cargo/cadastro";
		}
		
		if(cargo.getId() == null) {
			attr.addAttribute("fail", "cargo não existe");
		}else if( cargoService.buscarPorId(cargo.getId()) == null) {
			attr.addAttribute("fail", "cargo não existe");
		}
		else {
		cargoService.editar(cargo);
		attr.addFlashAttribute("success", "cargo editado com sucesso");
		}
		return "redirect:/cargos/listar";
	}
	
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Integer id, RedirectAttributes attr) {
		if(cargoService.cargoTemFuncionario(id)) {
			attr.addFlashAttribute("fail", "Cargo nao excluido. Tem funcionario vinculado");
		} else {
			cargoService.excluir(id);
			attr.addAttribute("success", "Cargo excluido com sucesso");
		}
		return "redirect:/cargos/listar";
	}
}
