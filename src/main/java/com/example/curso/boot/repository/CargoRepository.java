package com.example.curso.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.curso.boot.domain.Cargo;

@Repository
public interface CargoRepository extends JpaRepository<Cargo, Integer>{

}
