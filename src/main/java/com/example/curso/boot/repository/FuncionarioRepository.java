package com.example.curso.boot.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.curso.boot.domain.Funcionario;


@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer>{

	public List<Funcionario> findByNomeContaining(String nome);
	
	public List<Funcionario> findByCargoId(Integer id);
	
	public List<Funcionario> findByDataEntrada(LocalDate entrada);
	
	public List<Funcionario> findByDataSaida(LocalDate saida);
	
	public List<Funcionario> findByDataEntradaAfterAndDataSaidaBefore(LocalDate entrada, LocalDate saida);
}



