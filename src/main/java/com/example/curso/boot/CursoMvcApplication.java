package com.example.curso.boot;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoMvcApplication.class, args);
	}


}
